package com.example.demo;

import java.sql.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.modelo.entitys.Empleadoelek;
import com.example.demo.restController.EmpleadoelekController;

@SpringBootApplication
public class CrudExamenPracticoApplication {

	public static void main(String[] args) {
		 
		SpringApplication.run(CrudExamenPracticoApplication.class, args);
			
	}
}
