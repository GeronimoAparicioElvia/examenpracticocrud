package com.example.demo.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.modelo.entitys.Empleadoelek;
import com.example.demo.modelo.service.EmpleadoelekImpService;

@RestController
@RequestMapping (value="empleadoelek")
public class EmpleadoelekController {
	@Autowired
	private EmpleadoelekImpService empleadoelekImpS;
	
	//Regresa toda la lista de empleados
	@GetMapping(value ="listaEmpleados")
	public List <Empleadoelek> getEmpleadoselek(){
		return empleadoelekImpS.getEmpleadoselek();
	}
	
	//Retorna un empleadoelek por su id
	@GetMapping(value ="Empleado/{id}")
	public Empleadoelek getEmpleadoelek(@PathVariable Long id){
		return empleadoelekImpS.getEmpleado(id);
	}
	
	//Inserta un empleadoelek
	@PostMapping("insertEmpleado")
	public void insertEmpleadoelek(@RequestBody Empleadoelek empleado) {
		empleadoelekImpS.saveEmpleado(empleado);
	}
	
	//Actualiza el nombre de un empleadoelek pasandole como parametro id
	@PutMapping("updateEmpleado")
	public void updateEmpleadoelek(@RequestParam Long id, @RequestParam String nombre) {
		Empleadoelek emp = empleadoelekImpS.getEmpleado(id);
		emp.setNombre(nombre);
		empleadoelekImpS.saveEmpleado(emp);	
	}
	
	//Elimina a un empleado por su id
	@DeleteMapping("deleteEmpleado")
	public void deleteEmpleadoelek(@RequestParam Long id) {
		empleadoelekImpS.deleteEmpleado(id);
	}
	

	
	
	

}
