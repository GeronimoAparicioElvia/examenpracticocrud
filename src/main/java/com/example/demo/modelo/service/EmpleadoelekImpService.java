package com.example.demo.modelo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.modelo.dao.EmpleadoelekDao;
import com.example.demo.modelo.entitys.Empleadoelek;

@Service 
public class EmpleadoelekImpService implements EmpleadoelekService{
	@Autowired
	private EmpleadoelekDao empleadoelekDao;

	@Override
	public List<Empleadoelek> getEmpleadoselek() {
		return (List<Empleadoelek>) empleadoelekDao.findAll();
	}

	@Override
	public Empleadoelek getEmpleado(Long id) {
		return  empleadoelekDao.findById(id).get();
	}

	@Override
	public void saveEmpleado(Empleadoelek empleado) {
		empleadoelekDao.save(empleado);
	}

	
	@Override
	public void deleteEmpleado(Long id) {
		empleadoelekDao.deleteById(id);
	}

}
