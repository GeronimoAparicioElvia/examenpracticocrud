package com.example.demo.modelo.service;

import java.util.List;

import com.example.demo.modelo.entitys.Empleadoelek;

public interface EmpleadoelekService {

	public List<Empleadoelek> getEmpleadoselek();
	public Empleadoelek getEmpleado(Long id);
	public void saveEmpleado(Empleadoelek empleado);
	public void deleteEmpleado(Long id);
}
